<?php
include '../src/Pipedrive.php';

$pipedrive = new Pipedrive('546ec154d3def25072cec50a44c9a562e010e6a4');

/**
 * Sample to add a deal with person and organiyation and note
 * 
 */
$dealWithNotes = array(
    'title' => 'Deal create via API with notes',
    'description' => 'Deal description',
    'organization' => array(
        'name' => 'Codehive ' . md5(microtime()),
    ),
    'person' => array(
        'name' => 'Zoltán Kovacs ' . md5(microtime()),
        'email' => 'zoltan.kovacs@codehive.hu',
        'phone' => '+36206619092',
        
     ),
    'notes' => array(
        'content' => 'notes ' . md5(microtime()), // Required field
        //'person_id' => null,  // Use null (or empty value) if you do not want to associate note with person
        //'deal_id' => null,
        //'org_id' => null   // If org_id field is missing than associate not with same organization as deal
    ),  
);

$result = $pipedrive->addDeal($dealWithNotes);


/**
 * Sample to add a new note in separate process
 * The possible fields are:
 *      content  : required, html content or an array with key value pairs
 *      deal_id  : not required, if you want to assicate note with a deal
 *      org_id   : not required, if you want to assicate note with an organization
 *      person_id: not required, if you want to assicate note with a person
 * 
 * The addDeal result contains the appropriate deal_id, org_id, person_id.
 */

$notesResult = $pipedrive->addNotes(array(
//    'content' => array(           // It possible array content
//        'field1' => 'value1',
//        'field2' => 'value2',
//        'field3' => 'value3',
//    ),
    'content' => '<strong>field1:</strong> value1<br/><strong>field2:</strong> value2', 
    'deal_id' => $result['id'],             // Optional
    'org_id' => $result['org_id'],          // Optional
    'person_id' => $result['person_id'],    // Optional
));



// List of current organization, person and deal fields
// 
//$client = $pipedrive->getCleint();
//
//$organizationContainer = new PipedriveFieldContainer($client, $client->getOrganizationFields()->getData());
//$organizationContainer->dump();
//
//$personContainer = new PipedriveFieldContainer($client, $client->getPersonFields()->getData());
//$personContainer->dump();
//
//$dealContainer = new PipedriveFieldContainer($client, $client->getDealFields()->getData());
//$dealContainer->dump();

exit;


