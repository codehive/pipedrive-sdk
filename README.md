# Pipedrive SDK

This repository contains a PHP SDK that allows you to access Pipedrive CRM from your PHP app.

## Requirements
The php CURL extension

## Usage

The base class of SDK **Pipedrive** waiting for one parameter, the Pipedrive API [token]: <https://app.pipedrive.com/settings#api>

### Create a new organization

```php
<?php
require_once('path_of/sdk/pipedrive-sdk/src/Pipedrive.php');

$pipedrive = new Pipedrive('xxxxxxxxxxxxxxxx');

$datas = array(    
        'name' => 'Codehive',
);

$pipedrive->addOrganization($datas);
```

### Create new contact person with organization
```php
<?php
require_once('path_of/sdk/pipedrive-sdk/src/Pipedrive.php');

$pipedrive = new Pipedrive('xxxxxxxxxxxxxxxx');

$datas = array(
    'name' => 'Zoltán Kovács',
    'email' => 'zoltan.kovacs@codehive.hu',
    'phone' => '+36206619092',
    'organization' => array(
        'name' => 'Codehive',
    ),
);

$pipedrive->addPerson($datas);
```
If the organization does not exist, attempt to create it.


### Create a new deal with a contact person
```php
<?php
require_once('path_of/sdk/pipedrive-sdk/src/Pipedrive.php');

$pipedrive = new Pipedrive('xxxxxxxxxxxxxxxx');

$datas = array(
    'title' => 'Deal created via API',
    'person' => array(
        'name' => 'Zoltán Kovács',
        'email' => 'zoltan.kovacs@codehive.hu',
        'organization' => array(
            'name' => 'Codehive Szociális Szövetkezet',
        ),
     ),
);

$pipedrive->addDeal($datas);
```
If the person or organization does not exist, attempt to create it.


### Using cusrom field
It is also possible to send Pipedrive custom fields via API. If we created a deal custom field e.g.: Description
the request data will be:
```php
$datas = array(
    'title' => 'Deal created via API',
    'description' => 'Deal description',
);
```
We use the custom field as normalized format. (lowercase and skip any non alfa-numeric character).
e.g.:

 * Postal code => postalcode 
 * First name   => firstname
 * Name 2      => name2
 * Terms & condition => termscondition

## License 
See the LICENSE file