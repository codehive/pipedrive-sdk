<?php

/**
 * PipedriveClient.php
 *
 * PHP versions 5.3+
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 * 
 */

require_once 'PipedriveClientInterface.php';
require_once 'PipedriveHttpCurlClient.php';
require_once 'PipedriveResponse.php';

/**
 *  PipedriveClient
 * 
 * @author   Zoltán Kovács <zoltan.kovacs@codehive.hu>
 * @link     PipedriveClient
 */
class PipedriveClient implements PipedriveClientInterface
{
    const API_URI_PREFIX = 'https://api.pipedrive.com/v1';
    const API_TOKEN_KEY = 'api_token';
    
    protected $token = '';
    
    protected $isConnected = false;
    
    /**
     *
     * HttpClient adapter
     * 
     * @var PipedriveHttpClientInterface 
     */
    protected $httpClientAdapter;
    
    public function addDeal(array $datas)
    {
        $response =  $this->sendData($this->getUri('deals'), 'POST', $datas);
        return new PipedriveResponse($response);
    }

    public function addOrganization(array $datas)
    {
        $response =  $this->sendData($this->getUri('organizations'), 'POST', $datas);
        return new PipedriveResponse($response);
    }
    
    public function addNotes(array $datas)
    {
        $response = $this->sendData($this->getUri('notes'), 'POST', $datas);
        return new PipedriveResponse($response);
    }

    public function addPerson(array $datas)
    {
        $response =  $this->sendData($this->getUri('persons'), 'POST', $datas);
        return new PipedriveResponse($response);
    }

    public function findOrganization(array $datas)
    {
        if (!isset($datas['name'])) {
            throw new InvalidArgumentException('Find organization parameter must be contains a name value');
        }
        
        $params = array(
            'term' => $datas['name'],
            'start' => array_key_exists('start', $datas) ? $datas['start'] : 0,    
        );
        
        $response = $this->sendData($this->getUri('organizations/find'), 'GET', $params);
        
        return new PipedriveResponse($response);
    }

    public function findPerson(array $datas)
    {
        if (!isset($datas['name'])) {
            throw new InvalidArgumentException('Find person parameter must be contains a name value');
        }
        
        $params = array(
            'term' => $datas['name'],
            'start' => array_key_exists('start', $datas) ? $datas['start'] : 0,    
            'org_id' => array_key_exists('org_id', $datas) ? $datas['org_id'] : 0,
        );
        
        $response = $this->sendData($this->getUri('persons/find'), 'GET', $params);
        return new PipedriveResponse($response);
    }

    public function getDealFields()
    {
        $response = $this->sendData($this->getUri('dealFields'), 'GET');
        return new PipedriveResponse($response);
    }

    public function getOrganizationFields()
    {
        $response = $this->sendData($this->getUri('organizationFields'), 'GET');
        return new PipedriveResponse($response);
    }

    public function getPersonFields()
    {
        $response = $this->sendData($this->getUri('personFields'), 'GET');
        return new PipedriveResponse($response);
    }  
    
    public function getStages() 
    {
        $response = $this->sendData($this->getUri('stages'), 'GET');
        return new PipedriveResponse($response);
    }
    
    public function findStage($name, $pipline = null)
    {
        $stages = $this->getStages();
        foreach ($stages->getData() as $stage) {
            if (isset($stage['name']) && $stage['name'] == $name) {
                if ($pipline === null || $stage['pipline_name'] == $pipline) {
                    return $stage;
                }
            }
        }
        
        return null;
    }
    
    /**
     *  Set Pipedrive API token
     * 
     * @param string $token
     * @return PipedriveClient
     */
    public function setToken($token)
    {
        $this->token = (string) $token;
        return $this;
    }
    
    /**
     * Set HTTP client adapter
     * 
     * @param PipedriveHttpClientInterface $httpClient
     * @return PipedriveClient
     */
    public function setHttpClient(PipedriveHttpClientInterface $httpClient)
    {
        $this->httpClientAdapter = $httpClient;
        return $this;
    }
    
    /**
     *  Helper method for using Pipedrive REST API
     * 
     * @param string $uri
     * @param sring $method
     * @param array $datas
     * @return string
     */
    protected function sendData($uri, $method, $datas = null)
    {
        $headers = array('Accept' => 'application/json');
        $body = array();
        
        if (($method === 'GET' || $method === 'DELETE') && is_array($datas)) {
            $uri .= (strpos($uri, '?') === false ? '?' : '&') . http_build_query($datas);
            $body = '';
        }
        else if ($method === 'POST' || $method === 'PUT') {
            $body = json_encode($datas);
            $headers['Content-Type'] = 'application/json';
        }
        
        try {
            $this->httpClientAdapter->connect(self::API_URI_PREFIX);
            $this->httpClientAdapter->write($method, $uri, $headers, $body);
            $result = $this->httpClientAdapter->read();
            $this->httpClientAdapter->close();
            return $result;
        } catch (Exception $e) {
            $this->httpClientAdapter->close();
            throw $e;
        }
    }
    
    /**
     * Helper method for generate appropriate uri
     * 
     * @param string $action
     * @return string
     */
    protected function getUri($action)
    {
        return sprintf(
                '%s/%s%s%s=%s',
                rtrim(self::API_URI_PREFIX, DIRECTORY_SEPARATOR),
                ltrim($action, DIRECTORY_SEPARATOR),
                (strpos($action, '?') === false) ? '?' : '&',
                self::API_TOKEN_KEY,
                $this->token
        );
    }
}
