<?php

/**
 * PipedriveHttpCurlClient.php
 *
 * PHP versions 5.3+
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 * 
 */

require_once 'PipedriveHttpClientInterface.php';

/**
 *  PipedriveHttpCurlClient
 * 
 * @author   Zoltán Kovács <zoltan.kovacs@codehive.hu>
 * @link     PipedriveHttpCurlClient
 */
class PipedriveHttpCurlClient implements PipedriveHttpClientInterface
{
    
    /**
     * Config parameters
     * 
     * @var array
     */
    private $_config;
    
    /**
     * Curl session handle
     * 
     * @var resource|null
     */
    private $_curl;
    
    /**
     * Response gottent from server
     * 
     * @var string
     */
    private $_response;
    
    /**
     * Constructor
     *  
     * Config is set using setOptions() method
     * 
     * @throws \DomainException
     */
    public function __construct()
    {
        if (!extension_loaded('curl')) {
            throw new RuntimeException('cURL extension has to be loaded.');
        }
    }
    
    /**
     * Close open connection
     */
    public function close()
    {
        if (is_resource($this->_curl)) {
            curl_close($this->_curl);
        }
        
        $this->_curl = null;
    }

    
    /**
     *  Initalize curl
     * 
     * @param string $host
     * @param int $port
     * @throws \RuntimeException
     */
    public function connect($host, $port = 80)
    {
        if ($this->_curl) {
            $this->close();
        }
        
        $this->_curl = curl_init();
        if ($port != 80) {
            curl_setopt($this->_curl, CURLOPT_PORT, $port);
        }
        
        if (isset($this->_config['timeout'])) {
            curl_setopt($this->_curl, CURLOPT_TIMEOUT, $this->_config['timeout']);
        }
        
        if (!$this->_curl) {
            $this->close();
            throw new RuntimeException('Unable to connect to "' . $host . ':' . $port . '"');
        }
    }

    /**
     * Return read response from server
     * 
     * @return string
     */
    public function read()
    {
        return $this->_response;
    }

    /**
     * Set the configuration array
     * 
     * @param array $options
     */
    public function setOptions($options = array())
    {
        $this->_config = $options;
    }

    /**
     * Send a request to server
     * 
     * @param string $method    The request method (GET|POST|PUT|DELETE)
     * @param string $uri       The server uri
     * @param mixed $body       Request body (string or array)
     * @return string           The response
     * @throws RuntimeException
     * @throws InvalidArgumentException
     */
    public function write($method, $uri, $headers = array(), $body = '')
    {
        if (!$this->_curl) {
            throw new RuntimeException('Trying to write but we are not connect.');
        }
        
        
        curl_setopt($this->_curl, CURLOPT_URL, $uri);
        
        $curlValue = true;
        switch ($method) {
            case 'GET' :
                $curlMethod = CURLOPT_HTTPGET;
                break;
            case 'POST' :
                $curlMethod = CURLOPT_POST;
                break;
            case 'PUT' :
                $curlMethod = CURLOPT_CUSTOMREQUEST;
                $curlValue = 'PUT';
                break;
            case 'DELETE' :
                $curlMethod = CURLOPT_CUSTOMREQUEST;
                $curlValue = 'DELETE';
                break;
            default :
                throw new InvalidArgumentException(
                        'Method "' . $method . '" currently not supported.'
                );
        }
        
        curl_setopt($this->_curl, $curlMethod, $curlValue);
        curl_setopt($this->_curl, CURLOPT_RETURNTRANSFER, true);
        
        if ($method == 'POST' || $method == 'PUT') {
            curl_setopt($this->_curl, CURLOPT_POSTFIELDS, (string) $body);
        }
        
        if (!isset($headers['Accept'])) {
            $headers['Accept'] = '';
        }
        
        $curlHeaders = array();
        foreach ($headers as $key => $value) {
            $curlHeaders[] = $key . ': ' . $value;
        }
        
        curl_setopt($this->_curl, CURLOPT_HTTPHEADER, $curlHeaders);
        
        $this->_response = curl_exec($this->_curl);
        
        if (empty($this->_response)) {
            throw new RuntimeException('Error in cURL request: '  . curl_error($this->_curl));
        }
        
    }
    
}
