<?php

/**
 * PipedriveFieldMonetary.php
 *
 * PHP versions 5.3+
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 * @copyright Copyright (c) 2013 Codhive (http://codehive.hu)
 * 
 */

require_once 'PipedriveComposedField.php';

/**
 * PipedriveFieldMonetary
 * 
 * @license  MIT
 * @author   Zoltán Kovács <zoltan.kovacs@codehive.hu>
 * @link     PipedriveFieldMonetary
 */
class PipedriveFieldMonetary extends PipedriveComposedField
{
    
    protected function getKeySufffix()
    {
        return '_currency';
    }

}
