<?php

/**
 * PipedriveResponse.php
 *
 * PHP versions 5.2+
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 * 
 */


/**
 * PipedriveResponse
 * Represent a pipedrive API response
 * 
 * @author   Zoltán Kovács <zoltan.kovacs@codehive.hu>
 * @link     PipedriveResponse
 */
class PipedriveResponse
{
    /**
     * The response status
     * 
     * @var boolean
     */
    protected $success;
    
    /**
     * The response data
     * 
     * @var array
     */
    protected $data;
    
    /**
     * The response error message
     * 
     * @var string
     */
    protected $error;
    
    /**
     * Additional data 
     * 
     * @var array
     */
    protected $additionData;
    
    /**
     * Constructor
     * 
     * @param string $json  The json representation of pipedrive response
     */
    public function __construct($json)
    {
       $response = json_decode($json, true);
       
       if ($response === null) {
            $this->success = false;
            $this->error = 'Invalid json response';
       }
       
       else if (!isset($response['success'])) {
            $this->success = false;
            $this->error = 'The response not contains the success property';
       }
       
       else {
            $this->success = ($response['success'] === true) ? true : false;
            
            if (isset($response['error'])) {
                $this->error = $response['error'];
            }
            
            if (isset($response['data'])) {
                $this->data = $response['data'];
            }
            
            if (isset($response['additional_data'])) {
                $this->additionData = $response['additional_data'];
            }
       }
    }
    
    /**
     * Whether the response succcess? 
     * 
     * @return boolean
     */
    public function isSuccess()
    {
        return $this->success;
    }
    
    /** 
     * Get error message
     * 
     * @return string|null
     */
    public function getError()
    {
        return $this->error;
    }
    
    /**
     * Get response data
     * 
     * @return array|null
     */
    public function getData()
    {
        return $this->data;
    }        
    
    public function getOffset()
    {
        if (isset($this->additionData['pagination']['start'])) {
            return $this->additionData['pagination']['start'];
        }
        return null;
    }
    
    public function getLimit()
    {
        if (isset($this->additionData['pagination']['limit'])) {
            return $this->additionData['pagination']['limit'];
        }
        
        return null;
    }
    
    public function hasMoreItems()
    {
        if (isset($this->additionData['pagination']['more_items_in_collection'])) {
            return $this->additionData['pagination']['more_items_in_collection'];
        }
        
        return null;
    }
}
