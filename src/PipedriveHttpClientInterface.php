<?php

/**
 * PipedriveHttpClientInterface.php
 *
 * PHP versions 5.2+
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 * 
 */


/**
 *  PipedriveHttpClientInterface
 * 
 * @author   Zoltán Kovács <zoltan.kovacs@codehive.hu>
 * @link     PipedriveHttpClientInterface
 */
interface PipedriveHttpClientInterface
{
    /**
     *  Set the configuration array
     * 
     * @param array $options
     */
    public function setOptions($options = array());
    
    /**
     *  Connect to a remote server
     * 
     * @param string $host
     * @param int $port
     * @param bool $secure
     */
    public function connect($host, $port = 80);
    
    /**
     * Send a request to server
     * 
     * @param string $method
     * @param string $url
     * @param array $headers
     * @param mixed $body
     */
    public function write($method, $uri, $headers = array(), $body = '');
    
    /**
     * Read response from server
     * 
     * @return string
     */
    public function read();
    
    /**
     * Close the connection to server
     */
    public function close();
    
}
