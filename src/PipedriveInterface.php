<?php

/**
 * PipedriveInterface.php
 *
 * PHP versions 5.2+
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 * 
 */

/**
 *  PipedriveInterface
 *  Interface of Pipedrive sdk base class
 * 
 * @author   Zoltán Kovács <zoltan.kovacs@codehive.hu>
 * @link     PipedriveInterface
 */
interface PipedriveInterface
{
   /**
    * Add a new deal
    * 
    * @param array $datas   Deal properties as associative array
    * @return array         Created deal array
    */ 
   public function addDeal(array $datas);
   
   /**
    * Add a new person if not exists
    * 
    * @param array $datas   Person properties as associative array
    * @return array         Created or fetched array
    */
   public function addPerson(array $datas);
   
   /**
    * Add a new organization if not exists
    * 
    * @param array $datas
    * @return array         Created or fetched organization
    */
   public function addOrganization(array $datas);
   
}
