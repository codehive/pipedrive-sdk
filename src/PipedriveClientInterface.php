<?php

/**
 * PipedriveClient.php
 *
 * PHP versions 5.2+
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 * 
 */

require_once 'PipedriveResponse.php';
require_once 'PipedriveHttpClientInterface.php';

/**
 *  PipedriveClient
 * 
 *  Pipedrive API client object interface
 * 
 * @author   Zoltán Kovács <zoltan.kovacs@codehive.hu>
 * @link     PipedriveClient
 */
interface PipedriveClientInterface
{
    /**
     * Create a new deal
     * 
     * @param array $datas
     * @return PipedriveResponse
     */
    public function addDeal(array $datas);
    
    /**
     * Create a new person
     * 
     * @param array $datas
     * @return PipedriveResponse
     */
    public function addPerson(array $datas);
    
    /**
     * Create a new organization
     * 
     * @param array $datas
     * @return PipedriveResponse
     */
    public function addOrganization(array $datas);
    
    /**
     * Attache a note to deal
     * 
     * @param array $datas
     * @return PipedriveResponse
     */
    public function addNotes(array $datas);
    
    /**
     * Find a person with requested data
     * 
     * @param array $datas
     */
    public function findPerson(array $datas);
    
    /**
     * Find an organization
     * 
     * @param type $name
     * @return PipedriveResponse
     */
    public function findOrganization(array $datas);
    
    /**
     * Get all deal fields
     * 
     * @return PipedriveResponse
     */
    public function getDealFields();
    
    /**
     * Get all organization fields
     * 
     * @return PipedriveResponse
     */
    public function getOrganizationFields();
    
    /**
     * Get all person fields
     * 
     * @return PipedriveResponse
     */
    public function getPersonFields();
    
    /**
     *  Set Pipedrive API token
     * 
     * @param string $token
     * @return PipedriveClientInterface
     */
    public function setToken($token);
    
    /**
     *  Set http client adatper
     * 
     * @param PipedriveHttpClientInterface $httpClient
     * @return PipedriveClientInterface
     */
    public function setHttpClient(PipedriveHttpClientInterface $httpClient);
}
