<?php

/**
 * PipedriveFieldInterface.php
 *
 * PHP versions 5.2+
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 * 
 */

require_once 'PipedriveFieldContainer.php';

/**
 *  PipedriveFieldInterface
 *  
 *  Mapping between a pipedrive entity property key in API call and a data array
 *  
 * 
 * @author   Zoltán Kovács <zoltan.kovacs@codehive.hu>
 * @link     PipedriveFieldInterface
 */
interface PipedriveFieldInterface
{
    
    public function setDataKey($key);
    
    public function getValue();
    
    public function setValue($value);
    
    public function getName();

    public function getKey();
    
    public function getDataKey();
    
    public function getType();
    
    public function setContainer(PipedriveFieldContainer $container);
    
}
