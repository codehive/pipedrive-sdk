<?php

/**
 * PipedriveFieldContainer.php
 *
 * PHP versions 5.3+
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 * 
 */

require_once 'PipedriveFieldInterface.php';
require_once 'PipedriveField.php';
require_once 'PipedriveClient.php';
require_once 'PipedriveFieldStage.php';
require_once 'PipedriveFieldDateRange.php';
require_once 'PipedriveFieldMonetary.php';

/**
 *  PipedriveFieldContainer
 *  Store several PipedriveField instance
 * 
 * @author   Zoltán Kovács <zoltan.kovacs@codehive.hu>
 * @link     PipedriveFieldContainer
 */
class PipedriveFieldContainer
{
    /**
     * PipedriveFields
     * 
     * @var array
     */
    protected $fields;
    
    /**
     *
     * @var PipedriveClient
     */
    protected $client;
    
    /**
     *  Constructor
     * 
     * @param array $fields                 Fields array representation
     * @throws InvalidArgumentException     If the name or key propery does not exist in fileds parameter
     */
    public function __construct(PipedriveClient $client, array $fields = array())
    {
        $this->fields = array();
        $this->client = $client;
        
        foreach ($fields as $item) {
            $field = $this->factory($item);
            $this->addField($field);
        }
    }
    
    /**
     *  Get api client
     * 
     * @return PipedriveClient
     */
    public function getClient()
    {
        return $this->client;
    }
    
    /**
     *  Return true if there is field with the given dataKey
     * 
     * @param string $key   The requested dataKey
     * @return bool
     */
    public function hasKey($key)
    {
        return array_key_exists($key, $this->fields);
    }
    
    
    /**
     *  Add a new field to the container
     *  If the field exists do nothing
     * 
     * @param PipedriveFieldInterface $field
     * @return \PipedriveFieldContainer
     */
    public function addField(PipedriveFieldInterface $field)
    {
        if (!$this->hasKey($field->getDataKey())) {
            $this->fields[$field->getDataKey()] = $field;
            $field->setContainer($this);
        }
        
        return $this;
    }
    
    /**
     *  Return a PipedriveField object with given dataKey
     *  If the field does not exist return null
     * 
     * @param string $key DataKey
     * @return PipedriveField
     */
    public function getField($key)
    {
        if (!$this->hasKey($key)) {
            return null;
        }
        
        return $this->fields[$key];
    }
    
    public function getFields()
    {
        return $this->fields;
    }
    
    /**
     *  Get the first field with given key (not datakey)
     * 
     * @param string $key
     * @return null|PipedriveFieldInterface
     */
    public function getFieldByKey($key) 
    {
        /* @var $field PipedriveFieldInterface */
        foreach ($this->fields as $field) {
            if ($field->getKey() == $key) {
                return $field;
            }
        }
        
        return null;
    }
    
    
    /**
     *  Bind data array to fields 
     * 
     * @param array $data
     * @return \PipedriveFieldContainer
     */
    public function bind(array &$data)
    {
        foreach ($data as $key => $value) {
            $field = $this->getField($key);
            if ($field !== null) {
                $field->setValue($value);
            }
        }
        
        return $this;
    }
    
    /**
     *  Extract fields as key indexed array
     * 
     * @return array
     */
    public function extract()
    {
        $result = array();
        /* @var $field PipedriveField */
        foreach ($this->fields as $field) {
            $value = $field->getValue();
            if (!empty($value)) {
                $result[$field->getKey()] = $value;
            }
        }
        
        return $result;
    }
    
    /**
     * 
     * @param array $data
     * @return PipedriveFieldInterface
     * @throws DomainException
     */
    public function factory(&$data) 
    {
        // validate given data
        if (!isset($data['name']) || !isset($data['key']) || !isset($data['field_type'])) {
                throw new DomainException('Field item must be contains "name" and "key" property');
        }
        $type = $data['field_type'];
        $dataKey = $data['key'];
        
        // define appropriate class name
        $className = 'PipedriveField' . ucfirst(preg_replace('/[^\a-z]/i', '', $type));
        
        if (!class_exists($className)) {
            $className = 'PipedriveField';
        }
        
        // create field class
        /* @var $field PipedriveFieldInterface */
        $field = new $className($data['name'], $data['key'], $data['field_type']);
        
        // definde appropriate data key
        if (isset($data['dataKey'])) {
            $dataKey = $data['dataKey'];
        }
        else if (strlen($dataKey) > 32) {
            $dataKey = strtolower(preg_replace('/[^\da-z_]/i', '', str_replace(' ', '_', $data['name'])));
        }
        $field->setDataKey($dataKey);
        
        
        // add new filed to container
        return $field;
    }
    
    /**
     * Use it only for debug
     * 
     * Write all fields important data to the output
     * 
     * @return void
     */
    public function dump()
    {
        foreach ($this->fields as $field) {
            var_dump(array(
                'name' => $field->getName(),
                'type' => $field->getType(),
                'key' => $field->getKey(),
                'dataKey' => $field->getDataKey(),
            ));
        }     
    }
    
}
