<?php

/**
 * PipedriveFieldDateRange.php
 *
 * PHP versions 5.3+
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 * @copyright Copyright (c) 2013 Codhive (http://codehive.hu)
 * 
 */

require_once 'PipedriveComposedField.php';

/**
 * PipedriveFieldDateRange
 * 
 * @license  MIT
 * @author   Zoltán Kovács <zoltan.kovacs@codehive.hu>
 * @link     PipedriveFieldDateRange
 */
class PipedriveFieldDateRange extends PipedriveComposedField
{
    
    protected function getKeySufffix()
    {
        return '_until';
    }

    
}
