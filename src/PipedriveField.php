<?php

/**
 * PipedriveField.php
 *
 * PHP versions 5.2+
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 * 
 */

require_once 'PipedriveFieldInterface.php';
require_once 'PipedriveFieldContainer.php';

/**
 *  PipedriveField
 *  
 *  Mapping between a pipedrive entity property key in API call and a data array
 *  
 * 
 * @author   Zoltán Kovács <zoltan.kovacs@codehive.hu>
 * @link     PipedriveField
 */
class PipedriveField implements PipedriveFieldInterface
{
    
            
    /**
     * The name of this field
     * 
     * @var string
     */
    protected $name;
    
    /**
     * The key for API communication
     * 
     * @var string
     */
    protected $key;
    
    /**
     * The key for fetching value from array
     * 
     * @var string
     */
    protected $dataKey;
    
    
    /**
     * The field value
     * 
     * @var mixed
     */
    protected $value;
    
    /**
     * Pipedrive data type;
     * 
     * @var string
     */
    protected $type;
    
    /**
     *
     * @var PipedriveFieldContainer
     */
    protected $container;
    
    /**
     *  Constructor
     *  
     *  The default dataKey is same as the key property
     *  If the dataKey is differnt use the setDataKey method.
     * 
     * @param string $name
     * @param string $key
     */
    public function __construct($name, $key, $type)
    {
        $this->name = (string) $name;
        $this->key = (string) $key;
        $this->type = (string) ($type);
        $this->setDataKey($this->key);
    }
    
    /**
     *  Set dataKey
     * 
     * @param string $key
     * @return PipedriveField
     */
    public function setDataKey($key)
    {
        $this->dataKey = (string) $key;
        return $this;
    }
    
    /**
     *  Get value
     * 
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     *  Set value
     * 
     * @param mixed $value
     * @return PipedriveField
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }
    
    /**
     *  Get name
     * 
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get key
     *  
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * Get dataKey
     * 
     * @return string
     */
    public function getDataKey()
    {
        return $this->dataKey;
    }
    
    /**
     * Get type of field
     * 
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }
    
    /**
     * Set the owner container
     * 
     * @param PipedriveFieldContainer $container
     * @return \PipedriveField
     */
    public function setContainer(PipedriveFieldContainer $container)
    {
        $this->container = $container;
        return $this;
    }

}
