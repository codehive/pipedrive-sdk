<?php

/**
 * PipedriveFieldStage.php
 *
 * PHP versions 5.3+
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 * @copyright Copyright (c) 2013 Codhive (http://codehive.hu)
 * 
 */

require_once 'PipedriveField.php';

/**
 * PipedriveFieldStage
 * 
 * @license  MIT
 * @author   Zoltán Kovács <zoltan.kovacs@codehive.hu>
 * @link     PipedriveFieldStage
 */
class PipedriveFieldStage extends PipedriveField
{
    
    
    public function getKey()
    {
        return 'stage_id';
    }
    
    public function getValue() {
        if (!empty($this->value)) {
                $pipeline = $this->container->getFieldByKey('pipeline');
                $pipelineName = ($pipeline !== null ? $pipeline->getValue() : null );
                $stage = $this->container->getClient()->findStage($this->value, $pipelineName);
                return $stage['id'];
        }
        
        return null;
    }    
}
