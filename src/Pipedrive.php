<?php

/**
 * Pipedrive.php
 *
 * PHP versions 5.2+
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 * 
 */

require_once 'PipedriveInterface.php';
require_once 'PipedriveFieldContainer.php';
require_once 'PipedriveClient.php';
require_once 'PipedriveHttpCurlClient.php';

/**
 * Pipedrive
 * 
 * Pipedrive object using for add new deal, organization, person to Pipedrive CRM
 * 
 * 
 * @author   Zoltán Kovács <zoltan.kovacs@codehive.hu>
 * @link     PipedriveService
 */
class Pipedrive implements PipedriveInterface
{
    const API_KEY_DEAL_ID = 'deal_id';
    const API_KEY_ORGANIZATION_ID = 'org_id';
    const API_KEY_PERSON_ID       =  'person_id';
    const API_KEY_ENTITY_ID      = 'id';
    
    /**
     * API client
     * 
     * @var PipedriveClientInterface 
     */
    protected $client;
    
    /**
     * Pipedrive API token
     * @see https://app.pipedrive.com/settings#api
     * 
     * @var string
     */
    protected $token;
    
    /**
     * The key of person datas
     * 
     * @var string
     */
    protected $personDataKey = 'person';
    
    /**
     * The key of organization datas
     * 
     * @var string
     */
    protected $organizationDataKey = 'organization';
    
    /**
     * The key of notes datas
     * 
     * @var string
     */
    protected $notesDataKey = 'notes';
    
    /**
     * Deal fields
     * 
     * @var PipedriveFieldContainer
     */
    protected $dealFields;
    
    /**
     * Organization fields
     * 
     * @var PipedriveFieldContainer
     */
    protected $organizationFields;
    
    /**
     * Person fields
     * 
     * @var PipedriveFieldContainer
     */
    protected $personFields;
    
    
    /**
     * Constructor
     * 
     * @param string $token The Pipedrive API token
     */
    public function __construct($token)
    {
        $this->token = (string) $token;
        $this->client = new PipedriveClient();
        $this->client
                ->setToken($this->token)
                ->setHttpClient(new PipedriveHttpCurlClient());
    }
    
    /**
     * Change the default person data key
     * 
     * @param string $key The new key for person datas
     * @return Pipedrive Self instance
     */
    public function setPersonDataKey($key)
    {
        $this->personDataKey = $key;
        return $this;
    }
    
    /**
     * Set the http client adapter to own pipedrive cleint
     * 
     * @param PipedriveHttpClientInterface $client
     * @return Pipedrive Self instance
     */
    public function setHttpClientAdapter(PipedriveHttpClientInterface $client)
    {
        $this->client->setHttpClient($client);
        return $this;
    }
    
    /**
     * Change the default organization data key
     * 
     * @param string $key The new key of organization datas
     * @return PipedriveService
     */
    public function setOrganizationDataKey($key)
    {
        $this->organizationDataKey = $key;
        return $this;
    }

    public function addDeal(array $datas)
    {
        
        // Create or fetch organization entity
        if ((!array_key_exists(self::API_KEY_ORGANIZATION_ID, $datas)) && (isset($datas[$this->organizationDataKey]))) {
            if (empty($datas[$this->organizationDataKey]['name'])) {
                $datas[self::API_KEY_ORGANIZATION_ID] = null;
                        
            }
            $organization  = $this->addOrganization($datas[$this->organizationDataKey]);
            if (isset($organization[self::API_KEY_ENTITY_ID])) {
              $datas[self::API_KEY_ORGANIZATION_ID] = $organization[self::API_KEY_ENTITY_ID];
            }
        }
        
        // Create or fetch person entity
        if ((!array_key_exists(self::API_KEY_PERSON_ID, $datas)) && (isset($datas[$this->personDataKey]))) {
            // copy organization identifier to person datas if exists
            if (isset($datas[self::API_KEY_ORGANIZATION_ID])) {
                $datas[$this->personDataKey][self::API_KEY_ORGANIZATION_ID] = $datas[self::API_KEY_ORGANIZATION_ID];
            }
            
            $person = $this->addPerson($datas[$this->personDataKey]);
            
            if (isset($person[self::API_KEY_ENTITY_ID])) {
                $datas[self::API_KEY_PERSON_ID] = $person[self::API_KEY_ENTITY_ID];
            }
        }
        $this->getDealFields()->bind($datas);
        $result = $this->client->addDeal($this->dealFields->extract());
        if (!$result->isSuccess()) {
            throw new RuntimeException(
                    'Pipline API call error in create deal: "' . $result->getError() . '"'
            );
        }
        
        $resultData = $result->getData();
        
        if (isset($datas[$this->notesDataKey]) && isset($datas[$this->notesDataKey]['content'])) {
            $notesData = $datas[$this->notesDataKey];
            $this
                ->prepareNotesField(self::API_KEY_DEAL_ID, $notesData, $resultData, self::API_KEY_ENTITY_ID)
                ->prepareNotesField(self::API_KEY_PERSON_ID, $notesData, $datas)
                ->prepareNotesField(self::API_KEY_ORGANIZATION_ID, $notesData, $datas);
           
            $this->addNotes($notesData);
        }
        
        return $resultData;
    }
    
    public function addNotes(array $datas)
    {
        if (!isset($datas['content'])) {
            throw new InvalidArgumentException('The request datas must be contains "content" key');
        }
        $datas['content'] = $this->prepareNotesContent($datas['content']);
        $result = $this->client->addNotes($datas);
        
        if (!$result->isSuccess()) {
            throw new RuntimeException(
                    'Pipline API call error in create notes: "' . $result->getError() . '"'
            );
        }
        
        return $result->getData();
    }

    /**
     *  Create a new organization if does not exists.
     * 
     * @param array $datas
     * @return array
     * @throws RuntimeException
     */
    public function addOrganization(array $datas)
    {
        // check existing organization
        $organization = $this->getOrganization($datas);
        if ($organization !== null) {
            return $organization;
        }
        
        // create a new organization
        $this->getOrganizationFields()->bind($datas);
        $result = $this->client->addOrganization($this->organizationFields->extract());
        if (!$result->isSuccess()) {
            throw new RuntimeException(
                    'Pipline API call error in create organization: "' . $result->getError() . '"'
            );
        }
        
        return $result->getData();
    }

    /**
     * Create a new person in pipedrive
     * 
     * @param array $datas
     * @return array
     * @throws RuntimeException
     */
    public function addPerson(array $datas)
    {
        // Define organization identifer if neccessary and possible
        if ((array_key_exists('org_id', $datas) === false) && isset($datas[$this->organizationDataKey])) {
            if (empty($datas[$this->organizationDataKey]['name'])) {
                $datas[self::API_KEY_ORGANIZATION_ID] = null;
            }
            else {
                $organization = $this->addOrganization($datas[$this->organizationDataKey]);
                if (isset($organization['id'])) {
                    $datas[self::API_KEY_ORGANIZATION_ID] = $organization['id'];
                }
            }
        }
        
        // check existing person
        $person = $this->getPerson($datas);
        if ($person !== null) {
            return $person;
        }
        
        // creating a new person
        $this->getPersonFields()->bind($datas);
        $result = $this->client->addPerson($this->personFields->extract());
        if (!$result->isSuccess()) {
            throw new RuntimeException(
                    'Pipline API call error in create person: "' . $result->getError() . '"'
            );
        }
        
        return $result->getData();
    }
    
    /**
     * 
     * @return PipedriveClient
     */
    public function getCleint()
    {
        return $this->client;
    }
    

    /**
     * Get organization based on given datas
     * If the requested organization does not exists try to create it.
     * Return value is an associative array with organization details
     * 
     * @param array $datas
     * @return array
     * @throws InvalidArgumentException
     * @throws RuntimeException
     */
    protected function getOrganization(array $datas)
    {
        $result = $this->client->findOrganization($datas);
        
        if (!$result->isSuccess()) {
            throw new RuntimeException(
                    'Pipline API call error in find organization: "' . $result->getError() . '"'
            );
        }
        
        $datas = $result->getData();
        return $datas === null ? $datas : $datas[0];
    }
    
    /**
     * Get person based on given data
     * If the requested person does not exist try to create it.
     * Return value is an associative array with person details
     * 
     * @param array $datas
     * @return PipedriveFieldContainer
     * @throws InvalidArgumentException
     * @throws RuntimeException
     */
    protected function getPerson(array $datas)
    {
        if (!isset($datas['name'])) {
            throw new InvalidArgumentException(
                    'Person data must "' . print_r($datas, true) . '" be contains "name" field'
            );
        }
        
        $result = $this->client->findPerson($datas);
        
        if (!$result->isSuccess()) {
            throw new RuntimeException(
                    'Pipline API call error in find person: "' . $result->getError() . '"'
            );
        }
        
        $responseData = $result->getData();
        if ($responseData === null) {
            return null;
        }
        
        if (isset($datas['email'])) {
           foreach ($responseData as $personData) {
               if (isset($personData['email']) && ($personData['email'] === $datas['email'])) {
                   return $personData;
               }
           }
        }
        return $datas;
    }

    /**
     * Get the organizationFields container
     * 
     * @return PipedriveFieldContainer
     * @throws RuntimeException
     */
    protected function getOrganizationFields()
    {
        if ($this->organizationFields === null) {
            $result = $this->client->getOrganizationFields();
            if (!$result->isSuccess()) {
                throw new RuntimeException(
                        'Pipline API call error in getting organization fields: "' . $result->getError() . '"'
                );
            }
            
            $this->organizationFields = new PipedriveFieldContainer($this->client, $result->getData());
        }
        
        return $this->organizationFields;
    }
    
    /**
     * Get the dealFields container
     * 
     * @return PipedriveFieldContainer
     * @throws RuntimeException
     */
    protected function getDealFields()
    {
        if ($this->dealFields === null) {
            $result = $this->client->getDealFields();
            if (!$result->isSuccess()) {
                throw new RuntimeException(
                        'Pipline API call error in getting deal fields: "' . $result->getError() . '"'
                );
            }
            
            $this->dealFields = new PipedriveFieldContainer($this->client, $result->getData());
        }
        
        return $this->dealFields;
    }


    /**
     * Get the peronsFields container
     * 
     * @return PipedriveFieldContainer
     * @throws RuntimeException
     */
    protected function getPersonFields()
    {
        if ($this->personFields === null) {
            $result = $this->client->getPersonFields();
            if (!$result->isSuccess()) {
                throw new RuntimeException(
                        'Pipline API call error in getting person fields: "' . $result->getError() . '"'
                );
            }
            $this->personFields = new PipedriveFieldContainer($this->client, $result->getData());
        }
        
        return $this->personFields;
    }
    
    /**
     * Prepare notes field
     * If a speific field exists but empty it will be removed (not associate with notes)
     * 
     * @param string $key
     * @param array $desDatas
     * @param array $srcDatas
     * @param string $desKey
     * @return \Pipedrive
     */
    protected function prepareNotesField($key, &$desDatas, &$srcDatas, $desKey = null) 
    {
        if (array_key_exists($key, $desDatas)) {
            $data = $desDatas[$key];
            if (empty($data)) {
                $desDatas[$key] = 0;
            }
        }
        else {
            $desKey = ($desKey === null ? $key : $desKey);
            $desDatas[$key] = $srcDatas[$desKey];
        }
        
        return $this;
    }
    
    /**
     *  Prepared notes content
     *  Handle array type contents and convert to key: value list wich separete with new line
     * 
     * @param mixed $content
     * @return string
     */
    protected function prepareNotesContent($content)
    {
        $prepatedContent = '';
        if (is_string($content)) {
            return $content;
        }
        
        if (is_array($content)) {
            foreach ($content as $key => $value) {
                $prepatedContent .= $key . ': ' . $value . ' <br />';
            }
            
            return $prepatedContent;
        }
        
        return (string) $content;
    }
  
}
