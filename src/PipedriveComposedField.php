<?php

/**
 * PipedriveComposedField.php
 *
 * PHP versions 5.3+
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 * @copyright Copyright (c) 2013 Codhive (http://codehive.hu)
 * 
 */

require_once 'PipedriveField.php';

/**
 * PipedriveComposedField
 * 
 * @license  MIT
 * @author   Zoltán Kovács <zoltan.kovacs@codehive.hu>
 * @link     PipedriveComposedField
 */
abstract class PipedriveComposedField extends PipedriveField
{
    
    
    public function setValue($value)
    {
        if (!is_array($value)) {
            return parent::setValue($value);
        }
        $fromValue = array_shift($value);
        parent::setValue($fromValue);
        
        $untilValue = array_shift($value);
        $untilField = $this->container->getFieldByKey($this->key . $this->getKeySufffix());
        if ($untilField !== null) {
            $untilField->setValue($untilValue);
        }
        
        return $this;
    }
    
    abstract protected function getKeySufffix();
}
